package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.activities.RegisterActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.GatesEmployeeLogin.GatesLoginPojo;
import com.brainmagic.gatescatalog.api.models.MechanicAdd.Mechanicaddpojo;
import com.brainmagic.gatescatalog.api.models.checkregister.CheckRegister;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReatilerActivity extends AppCompatActivity {

    ImageView menu;
    private FloatingActionButton home,back;
    EditText mobileno_add_mechanic,name_add_mechanic,shop_name_add_mechanic,country_edit,state_add_mechanic,
            city_add_mechanic,zip_code_edit,street_edit,email_add_mechanic;
    String mobno,name,shopname,country,state,city,zipcode,streetname,email,gatesempname;
    Button add_mechanic;
    Alertbox alertbox;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reatiler);


        alertbox=new Alertbox(AddReatilerActivity.this);
        add_mechanic=findViewById(R.id.add_mechanic);
        email_add_mechanic=findViewById(R.id.email_add_mechanic);
        menu=findViewById(R.id.menu);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        mobileno_add_mechanic=findViewById(R.id.mobileno_add_mechanic);
        name_add_mechanic=findViewById(R.id.name_add_mechanic);
        shop_name_add_mechanic=findViewById(R.id.shop_name_add_mechanic);
        country_edit=findViewById(R.id.country_edit);
        state_add_mechanic=findViewById(R.id.state_add_mechanic);
        city_add_mechanic=findViewById(R.id.city_add_mechanic);
        zip_code_edit=findViewById(R.id.zip_code_edit);
        street_edit=findViewById(R.id.street_edit);

  gatesempname=myshare.getString("NameCheck","");
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(AddReatilerActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
//
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(AddReatilerActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(AddReatilerActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(AddReatilerActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(AddReatilerActivity.this, Price_Activity.class));
                                return true;
//                            case R.id.ptoductpop:
////                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
//                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(AddReatilerActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(AddReatilerActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(AddReatilerActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(AddReatilerActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(AddReatilerActivity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(AddReatilerActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

        mobileno_add_mechanic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileno_add_mechanic.length() == 10) {
                    mobno = mobileno_add_mechanic.getText().toString();
//                checkuserexists();
                    try {
                        final ProgressDialog loading = ProgressDialog.show(AddReatilerActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
                        APIService service = RetrofitClient.getApiService();

                        Call<CheckRegister> call = service.checkreg(mobno);
                        call.enqueue(new Callback<CheckRegister>() {
                            @Override
                            public void onResponse(Call<CheckRegister> call, final Response<CheckRegister> response) {
                                try {
                                    if (response.body().getResult().equals("Success")) {
                                        loading.dismiss();
                                     alertbox.showAlert("Mechanic Already Exist");
                                     mobileno_add_mechanic.setText("");
                                    }
                                    else if (response.body().getResult().equals("No Record")) {
                                        loading.dismiss();
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    loading.dismiss();
                                    alertbox.showAlertWithBack("Please Try Again Later");
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckRegister> call, Throwable t) {
                                loading.dismiss();
                                t.printStackTrace();
                                alertbox.showAlertWithBack(getString(R.string.server_error));
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        add_mechanic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mobno=mobileno_add_mechanic.getText().toString();
                name=name_add_mechanic.getText().toString();
                shopname=shop_name_add_mechanic.getText().toString();
                country=country_edit.getText().toString();
                state=state_add_mechanic.getText().toString();
                city=city_add_mechanic.getText().toString();
                zipcode=zip_code_edit.getText().toString();
                streetname=state_add_mechanic.getText().toString();
                email=email_add_mechanic.getText().toString();

                  if(mobno.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your mobile no", Toast.LENGTH_SHORT).show();
                }
               else if (mobno.length() != 10){
                    Toast.makeText(AddReatilerActivity.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                }

                else if(name.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your Name", Toast.LENGTH_SHORT).show();
                }

                else if(country.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your Country", Toast.LENGTH_SHORT).show();
                }
                else if(state.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your State", Toast.LENGTH_SHORT).show();
                }
                else if(city.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your City", Toast.LENGTH_SHORT).show();
                }
                else if(zipcode.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your Zipcode", Toast.LENGTH_SHORT).show();
                }
                else if(streetname.equals("")){
                    Toast.makeText(AddReatilerActivity.this, "Enter Your Address", Toast.LENGTH_SHORT).show();
                }
                else{
                    checkinternet();
                }
            }
        });
    }
    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(AddReatilerActivity.this);
        if (connection.CheckInternet()) {
            regiStration();
        }
        else {
            alertbox.showAlertWithBack(" Unable to connect Internet. please check your Internet connection ! ");

        }
    }
    private void regiStration() {
        final CustomProgressDialog progressDialog = new CustomProgressDialog(AddReatilerActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            APIService service = RetrofitClient.getApiService();
            Call<Mechanicaddpojo> call = service.addmechanic(mobno,name,email,state,city,zipcode,streetname,gatesempname);
            call.enqueue(new Callback<Mechanicaddpojo>() {
                @Override
                public void onResponse(Call<Mechanicaddpojo> call, Response<Mechanicaddpojo> response) {
                    progressDialog.dismiss();
                    try {
                        if (response.body().getResult().equals("Success")) {
                            progressDialog.dismiss();
                            alertbox.showAlert("Mechanic Successfully Added");
                             mobileno_add_mechanic.setText("");
                             name_add_mechanic.setText("");
                            email_add_mechanic.setText("");
                            shop_name_add_mechanic.setText("");
                            country_edit.setText("");
                            state_add_mechanic.setText("");
                            city_add_mechanic.setText("");
                            zip_code_edit.setText("");
                            street_edit.setText("");

                        }
                        else if (response.body().getResult().equals("NotSuccess")){
                            progressDialog.dismiss();
                            alertbox.showAlertWithBack("Something went wrong .Please try again later");
                        }
                        else {
                            progressDialog.dismiss();
                            alertbox.showAlertWithBack("No Record Found");
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<Mechanicaddpojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Something went wrong .Please try again later .");

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Something went wrong .Please try again later .");
        }
    }
}