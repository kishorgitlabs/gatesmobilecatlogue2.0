package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Details_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.Data;
import com.brainmagic.gatescatalog.adapter.HistoryPointsAdapter;
import com.brainmagic.gatescatalog.adapter.PriceListAdapter;
import com.brainmagic.gatescatalog.adapter.Product_Details_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.GatesEmployeeLogin.GatesLoginPojo;
import com.brainmagic.gatescatalog.api.models.GetPointHistory;
import com.brainmagic.gatescatalog.api.models.PointsDetailsPojo;
import com.brainmagic.gatescatalog.api.models.PriceList.PricePojo;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointsDetails extends AppCompatActivity {

    ImageView menu;
    FloatingActionButton home, back;


TextView one,two,three,four,five,six,seven,eight,nine;
  Alertbox alertbox;
  String mobno;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_details);

        one=findViewById(R.id.one);
        two=findViewById(R.id.two);
        three=findViewById(R.id.three);
        four=findViewById(R.id.four);
        five=findViewById(R.id.five);
        six=findViewById(R.id.six);
        seven=findViewById(R.id.seven);
        eight=findViewById(R.id.eight);
        nine=findViewById(R.id.nine);

        alertbox=new Alertbox(PointsDetails.this);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        mobno=myshare.getString("MobNo","");

        checkinternet();
        menu=findViewById(R.id.menu);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(PointsDetails.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
//
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(PointsDetails.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(PointsDetails.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(PointsDetails.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(PointsDetails.this, Price_Activity.class));
                                return true;
//                            case R.id.ptoductpop:
////                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
//                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(PointsDetails.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(PointsDetails.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(PointsDetails.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(PointsDetails.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(PointsDetails.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(PointsDetails.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

    }
    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(PointsDetails.this);

        if (connection.CheckInternet()) {
            getDetails();
        } else {

            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }


    private void getDetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(PointsDetails.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetrofitClient.getApiService();
            Call<PointsDetailsPojo> call = service.getdetails(mobno);
            call.enqueue(new Callback<PointsDetailsPojo>() {
                @Override
                public void onResponse(Call<PointsDetailsPojo> call, Response<PointsDetailsPojo> response) {
                    progressDialog.dismiss();
                    try{
                            if(response.body().getResult().equals("Success")) {
                                progressDialog.dismiss();

                              one.setText(response.body().getData().getWelcomebonus());
                                two.setText(response.body().getData().getJoiningfacebook());
                                three.setText(response.body().getData().getWeeklyquiz());
                                four.setText(response.body().getData().getTrainingattendance());
                                five.setText(response.body().getData().getTrainingassesment());
                                six.setText(response.body().getData().getMonthlyperformer());
                                seven.setText(response.body().getData().getCarpasting());
                                eight.setText(response.body().getData().getFlangeinstall());
                                nine.setText(response.body().getData().getTotpoints());

                            }
                            else if (response.body().getResult().equals("NotSuccess")){

                                alertbox.showAlertWithBack("No Record Found");
                            }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Something went wrong . Please try again later .");
                    }
                }
                @Override
                public void onFailure(Call<PointsDetailsPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Something went wrong . Please try again later .");
                }
            });
        }

}

