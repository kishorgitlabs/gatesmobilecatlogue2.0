package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gates.R;

public class SplashActivity extends Activity {

    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private boolean isregister;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        isregister = myshare.getBoolean("isregister", false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isregister) {
                    Intent mainIntent = new Intent(SplashActivity.this, HomeScreenActivity.class);
                    //  mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(mainIntent);

                    finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, RegisterActivity.class);
//                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(mainIntent);
                    finish();
                }


                /* Create an Intent that will start the Menu-Activity. */

            }
        }, 5000);
    }
}
