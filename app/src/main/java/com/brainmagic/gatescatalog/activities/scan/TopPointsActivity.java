package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.PriceListAdapter;
import com.brainmagic.gatescatalog.adapter.TopScoreAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.PriceList.PricePojo;
import com.brainmagic.gatescatalog.api.models.TopPointsPojo;
import com.brainmagic.gatescatalog.api.models.trainingvideos.TrainingVideosModel;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopPointsActivity extends AppCompatActivity {

    ImageView menu;
    FloatingActionButton home, back;
    ListView list_view;
Alertbox alertbox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_points);

        alertbox=new Alertbox(TopPointsActivity.this);
        menu=findViewById(R.id.menu);
        back = (FloatingActionButton) findViewById(R.id.back);
         home = (FloatingActionButton) findViewById(R.id.home);
        list_view=findViewById(R.id.list_view);



        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(TopPointsActivity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
//
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(TopPointsActivity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(TopPointsActivity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(TopPointsActivity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(TopPointsActivity.this, Price_Activity.class));
                                return true;
//                            case R.id.ptoductpop:
////                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
//                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(TopPointsActivity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(TopPointsActivity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(TopPointsActivity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(TopPointsActivity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(TopPointsActivity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(TopPointsActivity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

        update();

    }
    private void update(){
        Network_Connection_Activity connection = new Network_Connection_Activity(TopPointsActivity.this);

        if (connection.CheckInternet()) {
            getalldetails();
        } else {
            alertbox.showAlertWithBack(getString(R.string.internet_error_msg));
        }


    }

    private void getalldetails()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(TopPointsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        APIService service= RetrofitClient.getApiService();

        try {
            Call<TopPointsPojo> call = service.getDetails();
            call.enqueue(new Callback<TopPointsPojo>() {
                @Override
                public void onResponse(Call<TopPointsPojo> call, Response<TopPointsPojo> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()){
                            if(response.body().getResult().equals("Success")) {
                            TopScoreAdapter adapter=new TopScoreAdapter(TopPointsActivity.this,response.body().getData());
                            list_view.setAdapter(adapter);
                            }
                            else if (response.body().getResult().equals("NotSuccess")){
                                alertbox.showAlertWithBack("No Record Found");
                            }
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Something went wrong .Please try again later .");
                    }
                }

                @Override
                public void onFailure(Call<TopPointsPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlertWithBack("Something went wrong .Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertbox.showAlertWithBack("Something went wrong .Please try again later .");
        }
    }
}