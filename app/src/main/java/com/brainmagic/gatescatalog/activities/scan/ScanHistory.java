package com.brainmagic.gatescatalog.activities.scan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.ScanHistoryAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.sacnhistory.ScanHistoryResult;
import com.brainmagic.gatescatalog.api.models.sacnhistory.ScanHistoryList;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScanHistory extends Activity {

    private ListView scanhistory;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Mobno;
//    private ScanHistoryAdapter scanHistoryAdapter;
    private List<ScanHistoryResult> data;
    ProgressDialog progressDialog;
    ImageView back,menu;
    String Usertype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_history);
//        data=new ArrayList<>();

        scanhistory=findViewById(R.id.scan_history_list_view);
        back=findViewById(R.id.back);
        menu=findViewById(R.id.menu);
        myshare = getSharedPreferences("Registration",MODE_PRIVATE);
        editor = myshare.edit();

        Usertype=myshare.getString("Usertype","");
        Mobno=myshare.getString("MobNo","");
        scanHistory();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ScanHistory.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(ScanHistory.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(ScanHistory.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(ScanHistory.this, Price_Activity.class));
                                return true;
                            case R.id.ptoductpop:
//                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(ScanHistory.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(ScanHistory.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(ScanHistory.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(ScanHistory.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(ScanHistory.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(ScanHistory.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });
    }


    public void NointernetNOBACK(String s) {

        Alertbox alert=new Alertbox(ScanHistory.this);
        alert.NointernetNOBACK(s);
    }

    private void scanHistory()  {
        try {
            progressDialog  = progressDialog.show(ScanHistory.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();

            Call<ScanHistoryList> call = service.sacnhistory(Mobno);
            call.enqueue(new Callback<ScanHistoryList>() {
                @Override
                public void onResponse(Call<ScanHistoryList> call, Response<ScanHistoryList> response) {
                    try {
                        progressDialog.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data=response.body().getData();
                            if (data.size()==0){
                                Nointernet("You Do Not Have Scan History");
                            }
                            else {
                                ScanHistoryAdapter scanHistoryAdapter=new ScanHistoryAdapter(ScanHistory.this,data);
                                scanhistory.setAdapter(scanHistoryAdapter);
                            }
                        } else if (response.body().getResult().equals("AlreadScaned")) {
                            Nointernet("You Do Not Have Scan History");

                        }else {
                            Nointernet("You Do Not Have Scan History");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        Nointernet("Please Try Again Later");
                    }
                }
                @Override
                public void onFailure(Call<ScanHistoryList> call, Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                    Nointernet("Please Try Again Later");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Nointernet(String s) {

        Alertbox alert=new Alertbox(ScanHistory.this);
        alert.NointernetNOBACK(s);
    }
}
