package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.squareup.picasso.Picasso;

import java.io.File;


public class ZoomActivity extends Activity {

    ImageView expandedImageView;
    TextView close;
    private String product;

    private String Usertype,Useremail,Usermobile;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    Alertbox alertbox = new Alertbox(ZoomActivity.this);
    private String LocalPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_layout);
        LocalPath = "/data/data/" + getPackageName() + "/app_gates/gates/";

        expandedImageView = (ImageView) findViewById(R.id.zoomimage);
        close = (TextView) findViewById(R.id.okay);

        product = getIntent().getStringExtra("ImageName");


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();


        Usertype = myshare.getString("Usertype", "");
        Useremail = myshare.getString("Email","") ;
        Usermobile = myshare.getString("MobNo","") ;



        if(product.equals(""))
        {
            Picasso.with(ZoomActivity.this).load("file:///android_asset/noimagefound.jpg").into(expandedImageView);
        }
        else
        {
            Picasso.with(ZoomActivity.this).load(getImage(product)).into(expandedImageView);
        }

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                onBackPressed();
            }
        });

    }

    public File getImage(String imagename) {

        File mediaImage = null;
        Bitmap b = null ;
        try {
            // String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(LocalPath);
            if (!myDir.exists())
                return null;

            mediaImage = new File(myDir.getPath() + "/GatesProducts/"+imagename);
            if (mediaImage.getAbsolutePath() != null)
                b = BitmapFactory.decodeFile(mediaImage.getAbsolutePath());

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mediaImage;
    }


}