package com.brainmagic.gatescatalog.activities.scan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.Notification_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.GatesEmployeeLogin.GatesLoginPojo;
import com.brainmagic.gatescatalog.api.models.notification.NotificationModel;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GatesEmployeeLogin extends AppCompatActivity {

    ImageView menu;
    private FloatingActionButton home,back;
    private EditText username, password, newPassword;
    private String UserName, Password, getmobno;
    private Button loginbtn, changePassBtn;
    private Alertbox alertbox;
    AlertDialog alertDialog;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    private boolean isPasswordChange = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gates_employee_login);

        alertbox=new Alertbox(GatesEmployeeLogin.this);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        username = findViewById(R.id.user_name_gates);
        password = findViewById(R.id.password_gates);
        menu=findViewById(R.id.menu);
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        loginbtn = findViewById(R.id.login_gates);
        getmobno=myshare.getString("MobNo","");
//        forgetPassword = findViewById(R.id.forget_password);
//        didNotRecieveLogin=findViewById(R.id.did_not_recieve_details);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(GatesEmployeeLogin.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(GatesEmployeeLogin.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub

                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(GatesEmployeeLogin.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(GatesEmployeeLogin.this, About_Gate_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(GatesEmployeeLogin.this, Price_Activity.class));
                                return true;
//                            case R.id.ptoductpop:
////                                startActivity(new Intent(ScanHistory.this, ScanHistory.class));
//                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(GatesEmployeeLogin.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(ScanHistory.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(GatesEmployeeLogin.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(GatesEmployeeLogin.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(GatesEmployeeLogin.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(GatesEmployeeLogin.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(ScanHistory.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(GatesEmployeeLogin.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserName = username.getText().toString();
                Password = password.getText().toString();

                if (UserName.equals("") && Password.equals("")) {
                    Toast.makeText(GatesEmployeeLogin.this, "Enter user name and password", Toast.LENGTH_LONG).show();
                }
                else if (UserName.equals("")) {
//                    userNameGates.setFocusable(true);
                    username.setError("Enter User Name");
                }
                else if (Password.equals("")) {
//                    passwordGates.setFocusable(true);
                    password.setError("Enter Password");
                }
                else if(getmobno.equals(Password)){

                    checkinternet();
                }
             else{
                    alertbox.showAlert("Invalid username or password");
                }
            }
        });
    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(GatesEmployeeLogin.this);

        if (connection.CheckInternet()) {
            gateslogin();
        } else {

            alertbox.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    private void gateslogin() {
        final CustomProgressDialog progressDialog = new CustomProgressDialog(GatesEmployeeLogin.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            APIService service = RetrofitClient.getApiService();
            Call<GatesLoginPojo> call = service.gateslogin(Password);

            call.enqueue(new Callback<GatesLoginPojo>() {
                @Override
                public void onResponse(Call<GatesLoginPojo> call, Response<GatesLoginPojo> response) {
                    progressDialog.dismiss();
                    try {
                        if (response.body().getResult().equals("Success")) {
                            progressDialog.dismiss();

                            editor.putBoolean("gatesemplogin",true);
                            editor.commit();
                            Intent intent=new Intent(GatesEmployeeLogin.this,AddReatilerActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else if (response.body().getResult().equals("NotSuccess")){
                            progressDialog.dismiss();
                            alertbox.showAlert("Invalid username or password");

                        }
                        else {
                            progressDialog.dismiss();
                            alertbox.showAlertWithBack("No Record Found");


                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertbox.showAlertWithBack("Something went wrong . Please try again later .");

                    }
                }

                @Override
                public void onFailure(Call<GatesLoginPojo> call, Throwable t) {
                    progressDialog.dismiss();
                    alertbox.showAlert("Something went wrong . Please try again later .");

                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            alertbox.showAlertWithBack("Something went wrong . Please try again later .");
        }
    }
}