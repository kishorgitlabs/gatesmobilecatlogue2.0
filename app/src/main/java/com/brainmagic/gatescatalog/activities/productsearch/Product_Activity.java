package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.background.Backgroundpojo;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.services.BackgroundService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Product_Activity extends Activity {
    private RelativeLayout product_passengercar_relativelayout;
    private RelativeLayout product_hcv_relativelayout, product_update_relativelayout,
    product_oe_relativelayout,product_2w_relativelayout;
    private ImageView menu, search;
    private MultiAutoCompleteTextView searchBox;
//    private View productsearch_relativelayout;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country;
    private Alertbox box = new Alertbox(Product_Activity.this);
    private static final String TAG = "Product_Activity";
    private ViewGroup includeviewLayout;
    private TextView ProfileName;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton home,back;
    private ImageView profilePicture;
    private String[] countrytypes = {"Select Country", "All", "Brunei", "Cambodia", "India", "Indonesia", "Malaysia", "Myanmar", "Philippines", "Singapore", "Thailand", "Vietnam"};
    private String SelectedCountry;
    private ImageView info_Search,info_Clear;
    private ArrayList<String> PredictionList;
    private TextView passengerCarText,heavyCommercialText,wheelerText;
    private String nameLog,names;
    private String mobileNoLog;
    private String userTypeLog;
    private String stateLog;
    private String cityLog;
    private String zipCodeLog;
    private String locationLog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        product_passengercar_relativelayout = (RelativeLayout) findViewById(R.id.product_passenger_car_relativelayout);

        product_hcv_relativelayout = (RelativeLayout) findViewById(R.id.product_hcv_relativelayout);

        product_2w_relativelayout = (RelativeLayout) findViewById(R.id.product_2w_relativelayout);
        product_oe_relativelayout = (RelativeLayout) findViewById(R.id.product_oe_relativelayout);

//
        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);

        names=myshare.getString("NameCheck","");
        nameLog = myshare.getString("nameLogs", "");
        mobileNoLog = myshare.getString("MobNo", "");
        userTypeLog = myshare.getString("Usertype", "");
        stateLog = myshare.getString("state", "");
        cityLog = myshare.getString("city", "");
        zipCodeLog = myshare.getString("zipCode", "");
        locationLog = myshare.getString("streetName", "");

        editor = myshare.edit();

//
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
//
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Product_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
//                                startActivity(new Intent(Product_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Product_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


        product_passengercar_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


//                try {
//                    final ProgressDialog loading = ProgressDialog.show(Product_Activity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
//
//                    APIService service = RetrofitClient.getApiService();
//
//                    Call<Backgroundpojo> call = service.sendLog("Passenger Car & Light Commercial",names,"","",mobileNoLog,userTypeLog,stateLog,cityLog,zipCodeLog,locationLog,"Android");
//                    call.enqueue(new Callback<Backgroundpojo>() {
//                        @Override
//                        public void onResponse(Call<Backgroundpojo> call, Response<Backgroundpojo> response) {
//                            try {
//                                if (response.body().getResult().equals("Success")) {
//
//                                    loading.dismiss();
//                                    Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
//                                    a.putExtra("TYPE_OF_VEHICLE", "Passenger Car & Light Commercial");
//                                    a.putExtra("navitxt", "PC & LCV ");
//                                    a.putExtra("TITLE", "Passenger Car");
//                                    startActivity(a);
//
//                                }
//                                else if (response.body().getResult().equals("NotVerified")) {
//                                    loading.dismiss();
//                                }
//
//                            } catch (Exception e) {
//                                loading.dismiss();
//
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<Backgroundpojo> call, Throwable t) {
//                            t.printStackTrace();
//                            loading.dismiss();
//
//                        }
//                    });
//                } catch (Exception e) {
//
//                    e.printStackTrace();
//                }

//                sendLog("Segment","Passenger Car & Light Commercial");
//                sendLog("Passenger Car & Light Commercial",
//                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
//                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
                a.putExtra("TYPE_OF_VEHICLE", "Passenger Car & Light Commercial");
                a.putExtra("navitxt", "PC & LCV ");
                a.putExtra("TITLE", "Passenger Car");
                startActivity(a);
            }
        });


        product_2w_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

//                try {
//                    final ProgressDialog loading = ProgressDialog.show(Product_Activity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
//
//                    APIService service = RetrofitClient.getApiService();
//
//                    Call<Backgroundpojo> call = service.sendLog("2 Wheeler",names,"","",mobileNoLog,userTypeLog,stateLog,cityLog,zipCodeLog,locationLog,"Android");
//                    call.enqueue(new Callback<Backgroundpojo>() {
//                        @Override
//                        public void onResponse(Call<Backgroundpojo> call, Response<Backgroundpojo> response) {
//                            try {
//                                if (response.body().getResult().equals("Success")) {
//
//                                    loading.dismiss();
//                                    Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
//                                    a.putExtra("TYPE_OF_VEHICLE", "2 Wheeler");
//                                    a.putExtra("navitxt", "2 Wheeler ");
//                                    a.putExtra("TITLE", "2 Wheeler ");
//                                    startActivity(a);
//                                }
//                                else if (response.body().getResult().equals("NotVerified")) {
//                                    loading.dismiss();
//                                }
//
//                            } catch (Exception e) {
//                                loading.dismiss();
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<Backgroundpojo> call, Throwable t) {
//                            t.printStackTrace();
//                            loading.dismiss();
//
//                        }
//                    });
//                } catch (Exception e) {
//
//                    e.printStackTrace();
//                }

//                sendLog("Segment","2 Wheeler");
//                sendLog("2 Wheeler",
//                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
//                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
                a.putExtra("TYPE_OF_VEHICLE", "2 Wheeler");
                a.putExtra("navitxt", "2 Wheeler ");
                a.putExtra("TITLE", "2 Wheeler ");
                startActivity(a);
            }
        });

        product_oe_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendLog("OE/Gates/cross Ref Number Search",
////                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
////                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_OE_Activity.class);
                startActivity(a);
            }
        });

        product_hcv_relativelayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//
//                try {
//                    final ProgressDialog loading = ProgressDialog.show(Product_Activity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
//
//                    APIService service = RetrofitClient.getApiService();
//
//                    Call<Backgroundpojo> call = service.sendLog("Heavy Commercial",names,"","",mobileNoLog,userTypeLog,stateLog,cityLog,zipCodeLog,locationLog,"Android");
//                    call.enqueue(new Callback<Backgroundpojo>() {
//                        @Override
//                        public void onResponse(Call<Backgroundpojo> call, Response<Backgroundpojo> response) {
//                            try {
//                                if (response.body().getResult().equals("Success")) {
//                                    loading.dismiss();
//                                    Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
//                                    a.putExtra("TYPE_OF_VEHICLE", "Heavy Commercial");
//                                    a.putExtra("navitxt", "HCV ");
//                                    a.putExtra("TITLE", "Heavy Commercial");
//                                    startActivity(a);
//                                }
//                                else if (response.body().getResult().equals("NotVerified")) {
//                                    loading.dismiss();
//
//
//                                }
//
//                            }
//                            catch (Exception e) {
//                                loading.dismiss();
//                                e.printStackTrace();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<Backgroundpojo> call, Throwable t) {
//                            t.printStackTrace();
//                            loading.dismiss();
//                        }
//                    });
//                }
//                catch (Exception e) {
//                    e.printStackTrace();
//                }

//                sendLog("Segment","Heavy Commercial");
//                sendLog("Heavy Commercial",
//                        nameLog,"", "", mobileNoLog, userTypeLog, stateLog, cityLog,
//                        zipCodeLog, locationLog, "Android");
                Intent a = new Intent(Product_Activity.this, Product_Make_List_Activity.class);
                a.putExtra("TYPE_OF_VEHICLE", "Heavy Commercial");
                a.putExtra("navitxt", "HCV ");
                a.putExtra("TITLE", "Heavy Commercial");
                startActivity(a);
            }
        });

    }


    private void sendLog(String segment, String userName, String vehicleName, String partNo, String mobileNumber, String userTypes, String stateLog,
                         String CityLog, String ZipCodeLog, String location, String deviceType)
    {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            startService(new Intent(Product_Activity.this, BackgroundService.class)
                    .putExtra("segmentLog", segment)
                    .putExtra("nameLog",userName)
                    .putExtra("vehicleNameLog",vehicleName)
                    .putExtra("partNoLog", partNo)
                    .putExtra("mobileNoLog",mobileNumber)
                    .putExtra("userTypeLog",userTypes)
                    .putExtra("stateLog", stateLog)
                    .putExtra("cityLog",CityLog)
                    .putExtra("zipCodeLog",ZipCodeLog)
                    .putExtra("locationLog", location)
                    .putExtra("deviceTypeLog",deviceType)
            );
        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);

    }

}
