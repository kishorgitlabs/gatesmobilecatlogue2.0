package com.brainmagic.gatescatalog.activities.productsearch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;

import com.brainmagic.gatescatalog.activities.Price_Activity;
import com.bumptech.glide.Glide;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.About_Gate_Activity;
import com.brainmagic.gatescatalog.activities.Contact_Details_Activity;
import com.brainmagic.gatescatalog.activities.Distributer_Activity;
import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.Notification_Activity;
import com.brainmagic.gatescatalog.activities.OtherLinksActivity;
import com.brainmagic.gatescatalog.activities.Schemes_Offers_Page;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.adapter.ListViewAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.StringListModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//import adapter.Passenger_Car_Adapter;

public class Product_Make_List_Activity extends Activity {


    private SQLiteDatabase db;
    private ListView listview;
    //    private MaterialSearchBar search_Box;
    private Cursor c;
    private Integer CheckVal = 0;
    private ArrayList<String> OemList;
    private ArrayList<Integer> OemListCount;
    private String typeofvehicle, title;
    //    private TextView textView2;
    //GridView gridview;
//    private ArrayList<String> Image_name;
//    private ArrayList<Bitmap> Imagelist;
//    private Bitmap bitmap;
    private ImageView menu, search;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private String Email, Mobno, Usertype, Country;
    private LinearLayout listviewLayout, listViewEmpty;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    private ProgressDialog loading;
    private Alertbox box = new Alertbox(Product_Make_List_Activity.this);
    private String selectQuery;
    private String makeSearch, strVal;
    private TextView ProfileName;
    private List<String> searchItem;
    //    private ConstraintLayout signout_relativelayout;
    private LinearLayout signout_relativelayout;
    private FloatingActionButton back, home;
    private ViewGroup includeviewLayout;
    private ImageView profilePicture;
    private TextView navicationtxt;
    //    private ImageView info_Search,info_Clear;
    private ListViewAdapter listViewAdapter;

    private static final String TAG = "Product_Make_List_Activ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_make);

        listview = (ListView) findViewById(R.id.listview);
        listviewLayout = findViewById(R.id.list_view_layout);
        listViewEmpty = findViewById(R.id.list_view_empty);

        back = (FloatingActionButton) findViewById(R.id.back);
        home = (FloatingActionButton) findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);
//        search_Box = (MaterialSearchBar) findViewById(R.id.text_search_box);
        includeviewLayout = findViewById(R.id.profile_logo);
        ProfileName = (TextView) includeviewLayout.findViewById(R.id.profilename);
        signout_relativelayout = findViewById(R.id.signout);
        signout_relativelayout.setVisibility(View.GONE);
        profilePicture = (ImageView) includeviewLayout.findViewById(R.id.profilepicture);
//        info_Search = (ImageView) findViewById(R.id.info_search);
//        info_Clear = (ImageView) findViewById(R.id.clear_search);
        //   search = (ImageView) findViewById(R.id.search);
        navicationtxt = (TextView) findViewById(R.id.navicationtxt);

        navicationtxt.setText(getIntent().getStringExtra("navitxt"));
        OemList = new ArrayList<String>();
        OemListCount = new ArrayList<Integer>();

        typeofvehicle = getIntent().getStringExtra("TYPE_OF_VEHICLE");


        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");
        Country = myshare.getString("Country", "");
        ProfileName.setText(myshare.getString("name", "") + " , " + Country);
        if (!myshare.getString("profile_path", "").equals(""))
            Glide.with(Product_Make_List_Activity.this)
                    .load(new File(myshare.getString("profile_path", "")))
                    .into(profilePicture);

        if (myshare.getBoolean("islogin", false))
            signout_relativelayout.setVisibility(View.VISIBLE);
        else
            signout_relativelayout.setVisibility(View.GONE);

        signout_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("islogin", false).commit();
                final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.Product_Make_List_Activity), getString(R.string.channel_network_sign_out), Snackbar.LENGTH_INDEFINITE);
                mySnackbar.setActionTextColor(getResources().getColor(R.color.red));
                mySnackbar.setAction(getString(R.string.okay_button), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mySnackbar.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                });
                mySnackbar.show();
            }
        });



        final ImageView downArrow = includeviewLayout.findViewById(R.id.down_arrow);
        final RelativeLayout userData = includeviewLayout.findViewById(R.id.user_data);
//        ViewGroup homeLayout=findViewById(R.id.profile_logo);

        downArrow.setOnClickListener(new View.OnClickListener() {
            boolean visible;

            @Override
            public void onClick(View v) {
//                TransitionManager.beginDelayedTransition(homeLayout);

                ChangeBounds changeBounds = new ChangeBounds();
                changeBounds.setDuration(600L);

                TransitionManager.beginDelayedTransition(includeviewLayout, changeBounds);

                visible = !visible;
//                expandOrCollapse(userData,visible);
                if (visible)
                    downArrow.animate().rotation(180).setInterpolator(new LinearInterpolator()).setDuration(500);
                else
                    downArrow.animate().rotation(0).setInterpolator(new LinearInterpolator()).setDuration(500);
                userData.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });


//        search_Box.addTextChangeListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//
//                strVal = search_Box.getText().toString();
////                if(strVal.isEmpty()){
////                    new Oemnames().execute();
////
////                }else
//                    {
//                    listViewAdapter.filter(strVal);
//
//                    listViewAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
        /*search_Box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                makeSearch = search_Box.getText().toString();

                new Searchitem().execute(makeSearch);


            }
        });*/
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(Product_Make_List_Activity.this, HomeScreenActivity.class);
                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(home);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Make_List_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Product_Make_List_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;



                            case R.id.aboutpop:
                                startActivity(new Intent(Product_Make_List_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Product_Make_List_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Product_Make_List_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Product_Make_List_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Product_Make_List_Activity.this, Notification_Activity.class));
                                return true;

                            case R.id.pricelist:
                                startActivity(new Intent(Product_Make_List_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Product_Make_List_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Product_Make_List_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Product_Make_List_Activity.this, Distributer_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Product_Make_List_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Product_Make_List_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Make_List_Activity.this, EditProfileActivity.class));
//                                return true;

                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("SEGMENT", typeofvehicle);

                Intent a = new Intent(Product_Make_List_Activity.this, Product_Model_List_Activity.class);
                a.putExtra("OEM", OemList.get(i).toString());
                a.putExtra("navitxt", navicationtxt.getText() + " > " + OemList.get(i).toString().trim());
                a.putExtra("MAKENAME", OemList.get(i).toString().trim());
                a.putExtra("SEGMENT", typeofvehicle);
                startActivity(a);
            }
        });




       /* Animation makeInAnimation = AnimationUtils.makeInAnimation(getBaseContext(), false);
        makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
                home.setVisibility(View.VISIBLE);
            }
        });

        Animation makeOutAnimation = AnimationUtils.makeOutAnimation(getBaseContext(), true);
        makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                home.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });


        if (home.isShown()) {
            home.startAnimation(makeOutAnimation);
            back.startAnimation(makeOutAnimation);
        }

        if (!home.isShown()) {
            home.startAnimation(makeInAnimation);
            back.startAnimation(makeInAnimation);
        }*/


        /*listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                home.show();
                back.show();
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                home.hide();
                back.hide();
            }



        });*/


//        new Oemnames().execute();
        checkInternet();
    }

    private void checkInternet() {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(Product_Make_List_Activity.this);
        if (network_connection_activity.CheckInternet()) {
            getMakeList();
        } else {
            box.showAlertWithBack("Unable to connect Internet. Please check your Internet connection !");
        }
    }

    public void expandOrCollapse(final View v, boolean exp_or_colpse) {
        TranslateAnimation anim = null;
        if (exp_or_colpse) {
            anim = new TranslateAnimation(0.0f, 0.0f, -v.getHeight(), 0.0f);
            v.setVisibility(View.VISIBLE);
        } else {
            anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, -v.getHeight());
            Animation.AnimationListener collapselistener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    v.setVisibility(View.GONE);
                }
            };

            anim.setAnimationListener(collapselistener);
        }

        // To Collapse
        //

        anim.setDuration(300);
        anim.setInterpolator(new AccelerateInterpolator(0.5f));
        v.startAnimation(anim);
    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (myshare.getBoolean("islogin", false))
//            signout_relativelayout.setVisibility(View.VISIBLE);
//        else
//            signout_relativelayout.setVisibility(View.GONE);

    }


    private void getMakeList() {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Product_Make_List_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();

            Call<StringListModel> call = service.getMakeList(typeofvehicle);

            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
                    progressDialog.dismiss();

                    try {

                        if (response.isSuccessful()) {
                            if (response.body().getResult().equals("Success")) {

                                if (response.body().getData().equals("") || response.body().getData() == null ||
                                        response.body().getData().size() == 0){
                                    listViewEmpty.setVisibility(View.VISIBLE);
                                    listviewLayout.setVisibility(View.GONE);
                                }else {
                                    listViewEmpty.setVisibility(View.GONE);
                                    listviewLayout.setVisibility(View.VISIBLE);
                                }

                                OemList.clear();
                                OemList.addAll(response.body().getData());
                                listViewAdapter = new ListViewAdapter(Product_Make_List_Activity.this, response.body().getData());
                                listview.setAdapter(listViewAdapter);

                            } else {
                                box.showAlertWithBack("No Record Found");
                            }
                        } else {
                            box.showAlertWithBack("Something went wrong . Please try again later .");
                        }
                    } catch (Exception e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        box.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }


                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    box.showAlertWithBack("Something went wrong .Please try again later .");
                }
            });
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
            box.showAlertWithBack("Something went wrong .Please try again later .");
        }

    }

//    class Oemnames extends AsyncTask<String, Void, String> {
//        private ProgressDialog loading;
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            OemList.clear();
//            loading = ProgressDialog.show(Product_Make_List_Activity.this, getString(R.string.searching), getString(R.string.please_wait), false, false);
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            AppDatabase appDb = getAppDatabase(Product_Make_List_Activity.this);
//            SQLiteHelper dbHelper = new SQLiteHelper(Product_Make_List_Activity.this);
//            db = dbHelper.getReadableDatabase();
//
//            if (typeofvehicle.equals("Passenger Car")) {
//                selectQuery = "select distinct Make  from product where Segment ='Passenger Car' or Segment ='Light Commercial' order by Make asc";
//            } else {
//                selectQuery = "select distinct Make  from product where Segment ='" + typeofvehicle + "' order by Make asc ";
//            }
//
//            Log.v("Query == ", selectQuery);
//
//            c = db.rawQuery(selectQuery, null);
//            if (c.moveToFirst()) {
//                do {
//                    OemList.add(c.getString(c.getColumnIndex("Make")));
//                    int count=0;
//                    if (typeofvehicle.equals("Passenger Car")) {
//                        count = appDb.ProductsDAO().getMakesNewProductsCount("Passenger Car","Light Commercial", c.getString(c.getColumnIndex("Make")));
//                    }
//                    else
//                    {
//                        count = appDb.ProductsDAO().getMakeNewProductsCount(typeofvehicle, c.getString(c.getColumnIndex("Make")));
//                    }
//
//                    OemListCount.add(count);
////                    String query="Select count(Make) from product where Segment='"+typeofvehicle+"' and Make ='"+c.getString(c.getColumnIndex("Make"))+"' and New=''";
//
//                    Log.d(TAG, "doInBackground: "+count);
//                }
//                while (c.moveToNext());
//
//                db.close();
//                c.close();
//                return "success";
//            } else {
//                db.close();
//                c.close();
//                return "no data";
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            loading.dismiss();
//            if (s.equals("success")) {
//                listViewAdapter=new ListViewAdapter(Product_Make_List_Activity.this, OemList,OemListCount, CheckVal,false);
//                listview.setAdapter(listViewAdapter);
//            } else {
//                box.showAlertWithBack(getString(R.string.no_data_found));
//            }
//        }
//    }
}
