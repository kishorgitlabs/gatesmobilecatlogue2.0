package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.SchemesPromoAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.schemespromo.SchemesPromoModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Schemes_Offers_Page extends Activity {



    Connection connection;
    Statement stmt;
    ProgressDialog progressDialog;
    ResultSet resultSet;
   // String exp_date,scheme_name,descri_ption;
    String password,Usertype,Email,Mobno,formattedDate;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    ListView scheme_listview;
    ImageView back,menu;
    private Alertbox alertBox;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schemes__offers__page);

        back=(ImageView)findViewById(R.id.back);
        menu=(ImageView)findViewById(R.id.menu);

        scheme_listview=(ListView)findViewById(R.id.scheme_listview);

        alertBox = new Alertbox(this);
      //  textViewflow=(TextView)findViewById(R.id.textViewflow);

       // scheme=getIntent().getStringExtra("SCHEME_NO");

        myshare = getSharedPreferences("Registration",MODE_PRIVATE);
        editor = myshare.edit();

        Email=myshare.getString("Email","");
        Usertype=myshare.getString("Usertype","");
        Mobno=myshare.getString("MobNo","");
        password=myshare.getString("PASSWORD","");
      //  Mobno=myshare.getString("MobNo","");
        // user=getIntent().getStringExtra("USER_TYPE");

        //textViewflow.setText(user);





        Date d = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(d);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());


        Email=myshare.getString("Email","");
        Mobno=myshare.getString("MobNo","");
        Usertype=myshare.getString("Usertype","");



        checkinternet();



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Schemes_Offers_Page.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Schemes_Offers_Page.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Schemes_Offers_Page.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Schemes_Offers_Page.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Schemes_Offers_Page.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
                            case R.id.scan:
                                startActivity(new Intent(Schemes_Offers_Page.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Schemes_Offers_Page.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
//                                startActivity(new Intent(Schemes_Offers_Page.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Schemes_Offers_Page.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Schemes_Offers_Page.this, Distributor_Search_Activity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Schemes_Offers_Page.this, Price_Activity.class));
                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Schemes_Offers_Page.this, Contact_Details_Activity.class));
                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });

    }




    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Schemes_Offers_Page.this);

        if (connection.CheckInternet()) {
            callApiforSchemes();

        } else {
            //Toast.makeText(Schemes_Offers_Page.this, "Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertBox.showAlertWithBack(" Unable to connect Internet. please check your Internet connection ! ");

        }


    }

    private void callApiforSchemes()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Schemes_Offers_Page.this);
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            APIService service = RetrofitClient.getApiService();

            String userType = myshare.getString("Usertype","");

            Call<SchemesPromoModel> call = service.getSchemesPromo(userType);

            call.enqueue(new Callback<SchemesPromoModel>() {
                @Override
                public void onResponse(Call<SchemesPromoModel> call, Response<SchemesPromoModel> response) {
                    progressDialog.dismiss();
                    try {
                        if(response.body().getResult().equals("Success"))
                        {
                            SchemesPromoAdapter adapter = new SchemesPromoAdapter(Schemes_Offers_Page.this, response.body().getData());
                            scheme_listview.setAdapter(adapter);

                        }else {
                            alertBox.showAlertWithBack("No Schemes Found for you");
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }

                }

                @Override
                public void onFailure(Call<SchemesPromoModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Something went wrong .Please try again later .");
                }
            });

        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Something went wrong . Please try again later .");
        }

    }





}


