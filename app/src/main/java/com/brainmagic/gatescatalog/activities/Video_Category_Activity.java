package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.ListViewAdapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.trainingvideos.TrainingVideosModel;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Video_Category_Activity extends Activity {

    ImageView menu;
    ListView listview;
    FloatingActionButton back,home;

    Connection connection;
    Statement stmt;
    ProgressDialog progressDialog;
    ResultSet resultSet;
    Alertbox alertBox;
    List<String> category;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno,Usertype;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video__category_);

        back=findViewById(R.id.back);
        home=findViewById(R.id.home);
        menu=(ImageView)findViewById(R.id.menu);

        listview=(ListView)findViewById(R.id.listview);

        myshare = getSharedPreferences("Registration",MODE_PRIVATE);
        editor = myshare.edit();

        Email=myshare.getString("Email","");
        Mobno=myshare.getString("MobNo","");
        Usertype=myshare.getString("Usertype","");

        alertBox = new Alertbox(Video_Category_Activity.this);

        checkinternet();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a = new Intent(Video_Category_Activity.this, Videos_Activity.class);
                a.putExtra("CATEGORY", category.get(i).toString());
                startActivity(a);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Video_Category_Activity.this, HomeScreenActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Video_Category_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Video_Category_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Video_Category_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Video_Category_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Video_Category_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Video_Category_Activity.this, Video_Category_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(Video_Category_Activity.this, ScanActivity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Video_Category_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Video_Category_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Video_Category_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Video_Category_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Video_Category_Activity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Video_Category_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Video_Category_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }
        });


    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Video_Category_Activity.this);

        if (connection.CheckInternet()) {
//            new BackroundRunning().execute();
            callApiForVideos();

        } else {
           // Toast.makeText(Video_Category_Activity.this, "Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertBox.showAlertWithBack("Unable to connect Internet. please check your Internet connection ! ");
        }


    }


//    class BackroundRunning extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            progressDialog = new ProgressDialog(Video_Category_Activity.this);
//            // spinner (wheel) style dialog
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Loading...");
//            progressDialog.show();
//            //   s1=mobilenumber.getText().toString();
//            //  s2=password.getText().toString();
//
//        }
//
//
//
//
//    @Override
//        protected String doInBackground(String... strings) {
//
//            Server_Connection_Activity server=new Server_Connection_Activity();
//            try {
//                connection=server.getconnection();
//
//                stmt=connection.createStatement();
//
//                String selectquery= "SELECT distinct VideoCatagoryname from Video where Activevideo='Active' and deleteflag='notdelete' and VideoCatagoryname !='Catalogues'";
//
//
//                resultSet = stmt.executeQuery(selectquery);
//
//
//                while (resultSet.next()) {
//                    category.add(resultSet.getString("VideoCatagoryname"));
//                }
//
//                if(category.isEmpty())
//                {
//                    return "empty";
//                }
//                resultSet.close();
//                connection.close();
//                stmt.close();
//
//                return "Sucess";
//
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                return "error";
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//            if (s.equals("Sucess")) {
//
//                listview.setAdapter(new ListViewAdapter(Video_Category_Activity.this
//                        ,category ));
//                progressDialog.dismiss();
//
//            }
//            else if(s.equals("empty"))
//            {
//                showdialog("Sorry! No videos available");
//            }
//
//
//            else {
//                showdialog(" Unable to connect Internet. please check your Internet connection ! ");
//            }
//        }
//    }


    private void callApiForVideos()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Video_Category_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<TrainingVideosModel> call = service.getTrainingVideos();

            call.enqueue(new Callback<TrainingVideosModel>() {
                @Override
                public void onResponse(Call<TrainingVideosModel> call, Response<TrainingVideosModel> response) {
                    progressDialog.dismiss();

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            category = response.body().getData();

                            listview.setAdapter(new ListViewAdapter(Video_Category_Activity.this
                                    ,response.body().getData()));
//                            listview.setAdapter(product_details);
                        }
                        else {
                            alertBox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<TrainingVideosModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Something went wrong .Please try again later .");

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Something went wrong .Please try again later .");
        }


    }







}


