package com.brainmagic.gatescatalog.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.activities.productsearch.whatsnew.WhatsNew_ApplicationActivity;
import com.brainmagic.gatescatalog.activities.scan.ScanActivity;
import com.brainmagic.gatescatalog.adapter.Videos_Adapter;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.VideosMoreDetails.TrainingMoreDetailResult;
import com.brainmagic.gatescatalog.api.models.VideosMoreDetails.TrainingMoreDetailsModel;
import com.brainmagic.gatescatalog.font.FontDesign;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.progressdialog.CustomProgressDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Videos_Activity extends Activity {
    ImageView menu;
    FloatingActionButton back,home;
    Connection connection;
    Statement stmt;
    ProgressDialog progressDialog;
    ResultSet resultSet;
    ArrayList<String> select_videoname, select_video_description, select_url, select_videoattachment;

    ListView listview;
    String category;
    FontDesign header;
    ArrayList<Bitmap> map;
    String[] urls = new String[50];
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String Email, Mobno, Usertype;
    private Videos_Adapter product_details;
    private Alertbox alertBox;
    private List<TrainingMoreDetailResult> data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);


        back =  findViewById(R.id.back);
        home =  findViewById(R.id.home);
        menu = (ImageView) findViewById(R.id.menu);

        header =  findViewById(R.id.flowtext);

        listview = (ListView) findViewById(R.id.listview);


        map = new ArrayList<Bitmap>();

        select_videoname = new ArrayList<String>();
        select_videoattachment = new ArrayList<String>();
        select_video_description = new ArrayList<String>();
        select_url = new ArrayList<String>();

        category = getIntent().getStringExtra("CATEGORY");

        header.setText(category);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();

        Email = myshare.getString("Email", "");
        Mobno = myshare.getString("MobNo", "");
        Usertype = myshare.getString("Usertype", "");

        alertBox = new Alertbox(Videos_Activity.this);

        checkinternet();


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent = new  Intent(Intent.ACTION_VIEW);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.get(i).getVideoURL()));
                startActivity(intent);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Videos_Activity.this, HomeScreenActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK)
                );
            }
        });


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Videos_Activity.this, view);

                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });


                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.homepop:
                                Intent home = new Intent(Videos_Activity.this, HomeScreenActivity.class);
                                home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(home);
                                return true;

                            case R.id.aboutpop:
                                startActivity(new Intent(Videos_Activity.this, About_Gate_Activity.class));
                                return true;
                            case R.id.ptoductpop:
                                startActivity(new Intent(Videos_Activity.this, Product_Activity.class));
                                return true;
                            case R.id.other_link_prod:
                                Intent a = new Intent(Videos_Activity.this, OtherLinksActivity.class);
                                startActivity(a);
                                return true;
//                            case R.id.vediopop:
//                                if (!Usertype.equals("")) {
//                                    Intent f = new Intent(Videos_Activity.this, Videos_Activity.class);
//                                    startActivity(f);
//                                }
//                                return true;
                            case R.id.scan:
                                startActivity(new Intent(Videos_Activity.this, ScanActivity.class));
                                return true;
                            case R.id.pricelist:
                                startActivity(new Intent(Videos_Activity.this, Price_Activity.class));
                                return true;
                            case R.id.notifipop:
                                startActivity(new Intent(Videos_Activity.this, Notification_Activity.class));
                                return true;
                            case R.id.promo_scheem_pop:
                                startActivity(new Intent(Videos_Activity.this, Schemes_Offers_Page.class));
                                return true;
                            case R.id.whatpop:
                                startActivity(new Intent(Videos_Activity.this, WhatsNew_ApplicationActivity.class));
                                return true;
                            case R.id.disnetpop:
                                startActivity(new Intent(Videos_Activity.this, Distributor_Search_Activity.class));
                                return true;
//                                if (Usertype.equals("Gates Employee")) {
//                                    Intent g = new Intent(Videos_Activity.this, LoginActivity.class).putExtra("from", "dist");
//                                    startActivity(g);
//                                } else {
//                                    box.showAlert(getString(R.string.not_authorized));
//                                }
//                                return true;
                            case R.id.contactpop:
                                startActivity(new Intent(Videos_Activity.this, Contact_Details_Activity.class));
                                return true;
//                            case R.id.editprofilepop:
//                                startActivity(new Intent(Product_Activity.this, EditProfileActivity.class));
//                                return true;

                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popmenu);
                popupMenu.show();


            }

        });


    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }


    /*  private void checkinternet() {
          Network_Connection_Activity connection = new Network_Connection_Activity(Videos_Activity.this);

          if (connection.CheckInternet()) {
              new BackroundRunning().execute();

          } else {
              Toast.makeText(Videos_Activity.this, "NO INTERNATE", Toast.LENGTH_SHORT).show();
          }


      }


      class BackroundRunning extends AsyncTask<String , Void , String> {

          @Override
          protected void onPreExecute() {
              super.onPreExecute();

              progressDialog = new ProgressDialog(Videos_Activity.this);
              // spinner (wheel) style dialog
              progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
              progressDialog.setCancelable(false);
              progressDialog.setMessage("Loading...");
              progressDialog.show();
              //   s1=mobilenumber.getText().toString();
              //  s2=password.getText().toString();

          }

          @Override
          protected String doInBackground(String... strings) {

              Server_Connection_Activity server=new Server_Connection_Activity();
              try {
                  connection=server.getconnection();

                  stmt=connection.createStatement();

                  String selectquery= "SELECT * from Video where Activevideo='Active' and VideoCatagoryname='"+ category +"' ";


                  resultSet = stmt.executeQuery(selectquery);


                  while (resultSet.next())
                  {
                     // select_catageory_name.add(resultSet.getString("VideoCatagoryname"));
                      select_videoname.add(resultSet.getString("Videoname"));

                      select_video_description.add(resultSet.getString("VideoDescription"));
                      select_url.add(resultSet.getString("videoURL"));



                  }

                  resultSet.close();
                  connection.close();
                  stmt.close();

                  return "Sucess";

              } catch (Exception e) {
                  e.printStackTrace();
                  return "error";
              }

          }

          @Override
          protected void onPostExecute(String s) {
              super.onPostExecute(s);
              progressDialog.dismiss();
              if (s.equals("Sucess")){

                  Videos_Adapter product_details;
                  product_details = new Videos_Adapter(Videos_Activity.this,select_videoname,select_video_description,select_url);

                  listview.setAdapter(product_details);
              }

              else {
                  showdialog("No InterNet");
              }
          }
      }




      public void showdialog(String s) {

          AlertConnectionMessage alert=new AlertConnectionMessage(Videos_Activity.this);
          alert.AlertDialogShow(s);
      }



  }



  */
    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(Videos_Activity.this);

        if (connection.CheckInternet()) {
//            new BackroundRunning().execute();
            if(category.equals("Other Links")) callApiForOtherLinks();
            else callApiForVideos();


        } else {
            // Toast.makeText(Videos_Activity.this, "Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            alertBox.showAlertWithBack(" Unable to connect Internet. please check your Internet connection ! ");
        }



    }

    private void callApiForVideos()
    {
        try {
            APIService service = RetrofitClient.getApiService();
            Call<TrainingMoreDetailsModel> call = service.postVideo(category);

            call.enqueue(new Callback<TrainingMoreDetailsModel>() {
                @Override
                public void onResponse(Call<TrainingMoreDetailsModel> call, Response<TrainingMoreDetailsModel> response) {

                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            data = response.body().getData();
                            product_details = new Videos_Adapter(Videos_Activity.this,response.body().getData());
                            listview.setAdapter(product_details);
                        }
                        else {
                            alertBox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<TrainingMoreDetailsModel> call, Throwable t) {
                    alertBox.showAlertWithBack("Something went wrong .Please try again later .");
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
            alertBox.showAlertWithBack("Something went wrong .Please try again later .");
        }
    }

    private void callApiForOtherLinks()
    {
        final CustomProgressDialog progressDialog =new CustomProgressDialog(Videos_Activity.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            APIService service = RetrofitClient.getApiService();
            Call<TrainingMoreDetailsModel> call = service.otherLinks();

            call.enqueue(new Callback<TrainingMoreDetailsModel>() {
                @Override
                public void onResponse(Call<TrainingMoreDetailsModel> call, Response<TrainingMoreDetailsModel> response) {

                    progressDialog.dismiss();
                    try{
                        if("Success".equals(response.body().getResult()))
                        {
                            data = response.body().getData();
                            product_details = new Videos_Adapter(Videos_Activity.this,response.body().getData());
                            listview.setAdapter(product_details);
                        }
                        else {
                            alertBox.showAlertWithBack("No Record Found");
                        }
                    }catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        alertBox.showAlertWithBack("Invalid Response. Please try again Later");
                    }
                }

                @Override
                public void onFailure(Call<TrainingMoreDetailsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    alertBox.showAlertWithBack("Something went wrong .Please try again later .");
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            e.printStackTrace();
            alertBox.showAlertWithBack("Something went wrong .Please try again later .");
        }
    }

//    class BackroundRunning extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//
//            progressDialog = new ProgressDialog(Videos_Activity.this);
//            // spinner (wheel) style dialog
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            progressDialog.setCancelable(false);
//            progressDialog.setMessage("Loading...");
//            progressDialog.show();
//            //   s1=mobilenumber.getText().toString();
//            //  s2=password.getText().toString();
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings)
//        {
//
//            Server_Connection_Activity server = new Server_Connection_Activity();
//            try {
//                connection = server.getconnection();
//
//                stmt = connection.createStatement();
//
//                String selectquery= "SELECT * from Video where Activevideo='Active' and deleteflag='notdelete' and VideoCatagoryname='"+ category +"' order by Video_id";
//
//
//                resultSet = stmt.executeQuery(selectquery);
//
//                while (resultSet.next()){
//                    // if (resultSet.next()) {
//                    select_videoname.add(resultSet.getString("Videoname"));
//                    select_videoattachment.add("http://gates.brainmagicllc.com/Uploads/Events/"+resultSet.getString("VideoAttachment").replaceAll(" ","%20"));
//                    select_video_description.add(resultSet.getString("VideoDescription"));
//                    select_url.add(resultSet.getString("videoURL"));
//
//                    //  whatsnew_model.add(resultSet.getString("EngineSpecification"));
//                    //  productattachment.add(resultSet.getString("Producttype"));
//                    // productname.add(re.getString(c.getColumnIndex("ProductName")));
//                    //  description.add(c.getString(c.getColumnIndex("Description")));
//                    //  activemessage.add(c.getString(c.getColumnIndex("ActiveMessage")));
//                    //  productattachment.add(c.getString(c.getColumnIndex("ProductAttachment")));
//
//
//
//                }
//
//                if(select_videoname.isEmpty())
//                {
//                    return "empty";
//                }
//
//                resultSet.close();
//                connection.close();
//                stmt.close();
//
//                return "Sucess";
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                return "error";
//            }
//
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//
//
//            if (s.equals("Sucess")) {
//                if(!select_videoattachment.isEmpty())
//                {
//                   /* for (int i = 0; select_videoattachment.size() > i; i++) {
//                        urls[i] = "http://gates.brainmagicllc.com/Uploads/Events/"+ select_videoattachment.get(i).toString();
//                        Log.v("url", urls[i]);
//                    }*/
//                    Videos_Adapter product_details;
//                    product_details = new Videos_Adapter(Videos_Activity.this,select_videoname,select_video_description,select_url,select_videoattachment);
//
//                    listview.setAdapter(product_details);
//                    progressDialog.dismiss();
//                }
//            }
//
//            else if(s.equals("empty"))
//            {
//                showdialog("No verified found !");
//            }
//            else {
//                showdialog(" Unable to connect Internet. please check your Internet connection ! ");
//            }
//
//         /*   New_Adapter adapter;
//            adapter = new New_Adapter(Whats_New_Activity.this, productname, Imagelist);
//
//            gridview.setAdapter(adapter);*/
//            // listview.setAdapter(new ArrayAdapter<String>(OEM_Activity.this,
//            //    android.R.layout.simple_list_item_1,vehicle ));
//        }
//
//
//    }


    public void showdialog(String s) {

        Alertbox alert = new Alertbox(Videos_Activity.this);
//        alert.Nointernet(s);
    }

    public void NointernetNOBACK(String s) {

        Alertbox alert = new Alertbox(Videos_Activity.this);
        alert.NointernetNOBACK(s);
    }


    private class GetXMLTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // Bitmap map = null;

            for (String url : urls) {
                //map = downloadImage(url);
                // url.replaceAll(" ", "%20");
                if (url != null)
                    map.add(downloadImage(url));
            }
            return "Sucess";
        }

        // Sets the Bitmap returned by doInBackground
        @Override
        protected void onPostExecute(String result) {

        }

        // Creates Bitmap from InputStream and returns it
        private Bitmap downloadImage(String url) {


            // url.replaceAll(" ", "%20");
            Bitmap bitmap = null;
            InputStream stream = null;
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 1;

            try {
                stream = getHttpConnection(url);
                bitmap = BitmapFactory.
                        decodeStream(stream, null, bmOptions);
                stream.close();
            } catch (IOException e1) {
                e1.printStackTrace();
                Log.v("Error", e1.getMessage());
            }
            return bitmap;
        }

        // Makes HttpURLConnection and returns InputStream
        private InputStream getHttpConnection(String urlString)
                throws IOException {
            InputStream stream = null;
            String realurl = urlString.replace(" ", "%20");
            URL url = new URL(realurl);
            URLConnection connection = url.openConnection();

            try {

                android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
                android.os.StrictMode.setThreadPolicy(policy);

                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();

                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return stream;
        }
    }
}



