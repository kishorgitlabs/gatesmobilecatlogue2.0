
package com.brainmagic.gatescatalog.api.models.sacnhistory;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ScanHistoryResult {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Datetime")
    private String mDatetime;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Latitude")
    private String mLatitude;
    @SerializedName("Longitude")
    private String mLongitude;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("PartNo")
    private String mPartNo;
    @SerializedName("ScanStatus")
    private String mScanStatus;
    @SerializedName("SerialNo")
    private String mSerialNo;
    @SerializedName("UserType")
    private String mUserType;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDatetime() {
        return mDatetime;
    }

    public void setDatetime(String datetime) {
        mDatetime = datetime;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

    public String getScanStatus() {
        return mScanStatus;
    }

    public void setScanStatus(String scanStatus) {
        mScanStatus = scanStatus;
    }

    public String getSerialNo() {
        return mSerialNo;
    }

    public void setSerialNo(String serialNo) {
        mSerialNo = serialNo;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
