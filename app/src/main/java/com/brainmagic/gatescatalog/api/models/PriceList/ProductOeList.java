package com.brainmagic.gatescatalog.api.models.PriceList;

import com.google.gson.annotations.SerializedName;

public class ProductOeList {

	@SerializedName("Article")
	private String article;

	@SerializedName("InsertDate")
	private String insertDate;

	@SerializedName("MRP")
	private String mRP;

	@SerializedName("MonthTill")
	private String monthTill;

	@SerializedName("Segment")
	private String segment;

	@SerializedName("TecDocStatus")
	private String tecDocStatus;

	@SerializedName("CarID")
	private String carID;

	@SerializedName("Articlegroup")
	private String articlegroup;

	@SerializedName("StatusDescription")
	private String statusDescription;

	@SerializedName("Modelcode")
	private String modelcode;

	@SerializedName("MonthFrom")
	private String monthFrom;

	@SerializedName("Modelrange")
	private String modelrange;

	@SerializedName("ApplicationAttributes")
	private String applicationAttributes;

	@SerializedName("ModelSort")
	private String modelSort;

	@SerializedName("CC")
	private String cC;

	@SerializedName("YearTill")
	private String yearTill;

	@SerializedName("Status")
	private String status;

	@SerializedName("Stroke")
	private double stroke;

	@SerializedName("Enginecode")
	private String enginecode;

	@SerializedName("Position")
	private String position;

	@SerializedName("VehicleInfo")
	private String vehicleInfo;

	@SerializedName("Make")
	private String make;

	@SerializedName("YearFrom")
	private String yearFrom;

	@SerializedName("KW")
	private String kW;

	@SerializedName("Type")
	private String type;

	@SerializedName("ARG")
	private String aRG;

	@SerializedName("Fuel")
	private String fuel;

	@SerializedName("DateFrom")
	private String dateFrom;

	@SerializedName("Model")
	private String model;

	@SerializedName("HMDnr")
	private String hMDnr;

	@SerializedName("Id")
	private int id;

	@SerializedName("Level1")
	private String level1;

	@SerializedName("DateTo")
	private String dateTo;

	public void setArticle(String article){
		this.article = article;
	}

	public String getArticle(){
		return article;
	}

	public void setInsertDate(String insertDate){
		this.insertDate = insertDate;
	}

	public String getInsertDate(){
		return insertDate;
	}

	public void setMRP(String mRP){
		this.mRP = mRP;
	}

	public String getMRP(){
		return mRP;
	}

	public void setMonthTill(String monthTill){
		this.monthTill = monthTill;
	}

	public String getMonthTill(){
		return monthTill;
	}

	public void setSegment(String segment){
		this.segment = segment;
	}

	public String getSegment(){
		return segment;
	}

	public void setTecDocStatus(String tecDocStatus){
		this.tecDocStatus = tecDocStatus;
	}

	public String getTecDocStatus(){
		return tecDocStatus;
	}

	public void setCarID(String carID){
		this.carID = carID;
	}

	public String getCarID(){
		return carID;
	}

	public void setArticlegroup(String articlegroup){
		this.articlegroup = articlegroup;
	}

	public String getArticlegroup(){
		return articlegroup;
	}

	public void setStatusDescription(String statusDescription){
		this.statusDescription = statusDescription;
	}

	public String getStatusDescription(){
		return statusDescription;
	}

	public void setModelcode(String modelcode){
		this.modelcode = modelcode;
	}

	public String getModelcode(){
		return modelcode;
	}

	public void setMonthFrom(String monthFrom){
		this.monthFrom = monthFrom;
	}

	public String getMonthFrom(){
		return monthFrom;
	}

	public void setModelrange(String modelrange){
		this.modelrange = modelrange;
	}

	public String getModelrange(){
		return modelrange;
	}

	public void setApplicationAttributes(String applicationAttributes){
		this.applicationAttributes = applicationAttributes;
	}

	public String getApplicationAttributes(){
		return applicationAttributes;
	}

	public void setModelSort(String modelSort){
		this.modelSort = modelSort;
	}

	public String getModelSort(){
		return modelSort;
	}

	public void setCC(String cC){
		this.cC = cC;
	}

	public String getCC(){
		return cC;
	}

	public void setYearTill(String yearTill){
		this.yearTill = yearTill;
	}

	public String getYearTill(){
		return yearTill;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setStroke(double stroke){
		this.stroke = stroke;
	}

	public double getStroke(){
		return stroke;
	}

	public void setEnginecode(String enginecode){
		this.enginecode = enginecode;
	}

	public String getEnginecode(){
		return enginecode;
	}

	public void setPosition(String position){
		this.position = position;
	}

	public String getPosition(){
		return position;
	}

	public void setVehicleInfo(String vehicleInfo){
		this.vehicleInfo = vehicleInfo;
	}

	public String getVehicleInfo(){
		return vehicleInfo;
	}

	public void setMake(String make){
		this.make = make;
	}

	public String getMake(){
		return make;
	}

	public void setYearFrom(String yearFrom){
		this.yearFrom = yearFrom;
	}

	public String getYearFrom(){
		return yearFrom;
	}

	public void setKW(String kW){
		this.kW = kW;
	}

	public String getKW(){
		return kW;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setARG(String aRG){
		this.aRG = aRG;
	}

	public String getARG(){
		return aRG;
	}

	public void setFuel(String fuel){
		this.fuel = fuel;
	}

	public String getFuel(){
		return fuel;
	}

	public void setDateFrom(String dateFrom){
		this.dateFrom = dateFrom;
	}

	public String getDateFrom(){
		return dateFrom;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setHMDnr(String hMDnr){
		this.hMDnr = hMDnr;
	}

	public String getHMDnr(){
		return hMDnr;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLevel1(String level1){
		this.level1 = level1;
	}

	public String getLevel1(){
		return level1;
	}

	public void setDateTo(String dateTo){
		this.dateTo = dateTo;
	}

	public String getDateTo(){
		return dateTo;
	}
}