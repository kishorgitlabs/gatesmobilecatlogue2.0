
package com.brainmagic.gatescatalog.api.models.moredetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class MoreDetailsModel {

    @SerializedName("data")
    private List<ModelDetailsResult> mData;
    @SerializedName("product")
    private List<EngineCodeResult> engineCodeResult;
    @SerializedName("data1")
    private List<VideoAttachment> mData1;
    @SerializedName("img")
    private List<ImageList> imageLists;
    @SerializedName("pdfval")
    private List<PDFModel> pdfModelsList;
    @SerializedName("result")
    private String mResult;

    public List<EngineCodeResult> getEngineCodeResult() {
        return engineCodeResult;
    }

    public void setEngineCodeResult(List<EngineCodeResult> engineCodeResult) {
        this.engineCodeResult = engineCodeResult;
    }


    public List<PDFModel> getPdfModelsList() {
        return pdfModelsList;
    }

    public void setPdfModelsList(List<PDFModel> pdfModelsList) {
        this.pdfModelsList = pdfModelsList;
    }

    public List<ImageList> getImageLists() {
        return imageLists;
    }

    public void setImageLists(List<ImageList> imageLists) {
        this.imageLists = imageLists;
    }

    public List<ModelDetailsResult> getData() {
        return mData;
    }

    public void setData(List<ModelDetailsResult> data) {
        mData = data;
    }

    public List<VideoAttachment> getData1() {
        return mData1;
    }

    public void setData1(List<VideoAttachment> data1) {
        mData1 = data1;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
