
package com.brainmagic.gatescatalog.api.models.sacnhistory;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class ScanHistoryList {

    @SerializedName("data")
    private List<ScanHistoryResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<ScanHistoryResult> getData() {
        return mData;
    }

    public void setData(List<ScanHistoryResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
