
package com.brainmagic.gatescatalog.api.models.whatsnew;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class WhatsNewModel {

    @SerializedName("data")
    private List<WhatsNewResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<WhatsNewResult> getData() {
        return mData;
    }

    public void setData(List<WhatsNewResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
