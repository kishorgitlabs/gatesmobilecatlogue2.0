package com.brainmagic.gatescatalog.api;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.brainmagic.gatescatalog.Constants.ROOT_URL;



/**
 * Created by SYSTEM10 on 6/7/2018.
 */

public class RetrofitClient {


    private static Retrofit getRetrofitInstance() {
        GsonConverterFactory gsonConverterFactory=GsonConverterFactory.create(new GsonBuilder()
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }



    private static Retrofit getRetrofitQualityInstance() {
        GsonConverterFactory gsonConverterFactory=GsonConverterFactory.create(new GsonBuilder()
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
    }


    /**
     * Get API Service
     *
     * @return API Service
     */
    public static APIService getApiService() {
        return getRetrofitInstance().create(APIService.class);
    }


//    public static APIService getApiServices() {
//        return getRetrofitInstances().create(APIService.class);
//    }


    public static APIService getApiQualityService() {
        return getRetrofitQualityInstance().create(APIService.class);
    }
}
