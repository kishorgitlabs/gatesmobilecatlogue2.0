package com.brainmagic.gatescatalog.api.models.versioncheck;

import com.google.gson.annotations.SerializedName;

public class VersionUpdate{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private String data;

	public String getResult(){
		return result;
	}

	public String getData(){
		return data;
	}
}