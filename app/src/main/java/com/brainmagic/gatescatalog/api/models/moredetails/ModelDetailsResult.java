
package com.brainmagic.gatescatalog.api.models.moredetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ModelDetailsResult {

    @SerializedName("make")
    private String mMake;
    @SerializedName("modelList")
    private List<ModelList> mModelList;

    public String getMake() {
        return mMake;
    }

    public void setMake(String make) {
        mMake = make;
    }

    public List<ModelList> getModelList() {
        return mModelList;
    }

    public void setModelList(List<ModelList> modelList) {
        mModelList = modelList;
    }

}
