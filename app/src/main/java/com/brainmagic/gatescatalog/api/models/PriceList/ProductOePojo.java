package com.brainmagic.gatescatalog.api.models.PriceList;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductOePojo{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<ProductOeList> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<ProductOeList> data){
		this.data = data;
	}

	public List<ProductOeList> getData(){
		return data;
	}
}