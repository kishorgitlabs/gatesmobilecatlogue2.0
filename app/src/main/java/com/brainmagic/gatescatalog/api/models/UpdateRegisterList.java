package com.brainmagic.gatescatalog.api.models;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("jsonschema2pojo")
public class UpdateRegisterList {


    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private UpdateRegister data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public UpdateRegister getData() {
        return data;
    }

    public void setData(UpdateRegister data) {
        this.data = data;
    }
}
