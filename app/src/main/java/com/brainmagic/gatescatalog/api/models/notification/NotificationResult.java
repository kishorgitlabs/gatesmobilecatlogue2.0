
package com.brainmagic.gatescatalog.api.models.notification;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class NotificationResult {

    @SerializedName("ActiveNotification")
    private String mActiveNotification;
    @SerializedName("date")
    private String mDate;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("NotificationExpiryDate")
    private Object mNotificationExpiryDate;
    @SerializedName("NotificationName")
    private String mNotificationName;
    @SerializedName("Notificationdetails_id")
    private Long mNotificationdetailsId;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("Usertype")
    private String mUsertype;

    public String getActiveNotification() {
        return mActiveNotification;
    }

    public void setActiveNotification(String activeNotification) {
        mActiveNotification = activeNotification;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Object getNotificationExpiryDate() {
        return mNotificationExpiryDate;
    }

    public void setNotificationExpiryDate(Object notificationExpiryDate) {
        mNotificationExpiryDate = notificationExpiryDate;
    }

    public String getNotificationName() {
        return mNotificationName;
    }

    public void setNotificationName(String notificationName) {
        mNotificationName = notificationName;
    }

    public Long getNotificationdetailsId() {
        return mNotificationdetailsId;
    }

    public void setNotificationdetailsId(Long notificationdetailsId) {
        mNotificationdetailsId = notificationdetailsId;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
