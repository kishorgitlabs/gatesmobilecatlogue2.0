package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuizGetClass {



    @SerializedName("questionno")
    @Expose
    private String questionno;
    @SerializedName("questionE")
    @Expose
    private String questionE;
    @SerializedName("questionH")
    @Expose
    private String questionH;
    @SerializedName("optionAE")
    @Expose
    private String optionAE;
    @SerializedName("optionAH")
    @Expose
    private String optionAH;
    @SerializedName("optionBE")
    @Expose
    private String optionBE;
    @SerializedName("optionBH")
    @Expose
    private String optionBH;
    @SerializedName("optionCE")
    @Expose
    private String optionCE;
    @SerializedName("optionCH")
    @Expose
    private String optionCH;
    @SerializedName("correctanswer")
    @Expose
    private String correctanswer;

    public String getQuestionno() {
        return questionno;
    }

    public void setQuestionno(String questionno) {
        this.questionno = questionno;
    }

    public String getQuestionE() {
        return questionE;
    }

    public void setQuestionE(String questionE) {
        this.questionE = questionE;
    }

    public String getQuestionH() {
        return questionH;
    }

    public void setQuestionH(String questionH) {
        this.questionH = questionH;
    }

    public String getOptionAE() {
        return optionAE;
    }

    public void setOptionAE(String optionAE) {
        this.optionAE = optionAE;
    }

    public String getOptionAH() {
        return optionAH;
    }

    public void setOptionAH(String optionAH) {
        this.optionAH = optionAH;
    }

    public String getOptionBE() {
        return optionBE;
    }

    public void setOptionBE(String optionBE) {
        this.optionBE = optionBE;
    }

    public String getOptionBH() {
        return optionBH;
    }

    public void setOptionBH(String optionBH) {
        this.optionBH = optionBH;
    }

    public String getOptionCE() {
        return optionCE;
    }

    public void setOptionCE(String optionCE) {
        this.optionCE = optionCE;
    }

    public String getOptionCH() {
        return optionCH;
    }

    public void setOptionCH(String optionCH) {
        this.optionCH = optionCH;
    }

    public String getCorrectanswer() {
        return correctanswer;
    }

    public void setCorrectanswer(String correctanswer) {
        this.correctanswer = correctanswer;
    }

}
