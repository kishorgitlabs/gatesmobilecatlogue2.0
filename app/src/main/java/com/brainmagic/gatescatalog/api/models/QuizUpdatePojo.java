package com.brainmagic.gatescatalog.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuizUpdatePojo {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private QuizupdateData data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public QuizupdateData getData() {
        return data;
    }

    public void setData(QuizupdateData data) {
        this.data = data;
    }
}
