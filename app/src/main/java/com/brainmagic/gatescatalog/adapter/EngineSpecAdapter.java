package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;

import java.util.List;

public class EngineSpecAdapter extends ArrayAdapter {
    private Context context;
    private List<EngineCodeResult> data;
    public EngineSpecAdapter(@NonNull Context context, List<EngineCodeResult> data) {
        super(context, R.layout.adapter_engine_spec);
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_engine_spec,parent,false);
        }

        TextView stroke = convertView.findViewById(R.id.stroke_value);
        TextView cc = convertView.findViewById(R.id.cc_value);
        TextView kw = convertView.findViewById(R.id.kw_value);
        TextView level = convertView.findViewById(R.id.level_value);
        TextView fuel = convertView.findViewById(R.id.fuel_value);


        stroke.setText(data.get(position).getStroke()+"");
        cc.setText(data.get(position).getCC());
        kw.setText(data.get(position).getKW());
        level.setText(data.get(position).getLevel1());
        fuel.setText(data.get(position).getFuel());


        return convertView;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
