package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.api.models.PriceList.OePartList;
import com.brainmagic.gatescatalog.api.models.PriceList.ProductOeList;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class ProductSearchAdapter extends ArrayAdapter {

    Context context;
    List<ProductOeList> data;

    public ProductSearchAdapter(@NonNull Context context, List<ProductOeList> data) {
        super(context, R.layout.productoesearchadapter);
        this.context=context;
        this.data=data;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.productoesearchadapter,parent,false);

        TextView make=(TextView)view.findViewById(R.id.oem);
        TextView model=(TextView)view.findViewById(R.id.model);
        TextView articelgroup=(TextView)view.findViewById(R.id.product_type);
        TextView segment=(TextView)view.findViewById(R.id.productdesc);
        TextView gatespartno=(TextView)view.findViewById(R.id.product_mrp);
        TextView mrp=(TextView)view.findViewById(R.id.segment);
        TextView morinfo=(TextView)view.findViewById(R.id.engine_specification);
        TextView enginecode=(TextView)view.findViewById(R.id.product_partno);
        TextView fueltype=(TextView)view.findViewById(R.id.fueltype);


      make.setText(data.get(position).getMake());
      model.setText(data.get(position).getModel());
      articelgroup.setText(data.get(position).getArticlegroup());
        segment.setText(data.get(position).getSegment());
        gatespartno.setText(data.get(position).getArticle());
        mrp.setText(data.get(position).getMRP());
        morinfo.setText(data.get(position).getModelcode());
        enginecode.setText(data.get(position).getEnginecode());
        fueltype.setText(data.get(position).getFuel());




return view;

    }
    public int getCount() {
        return data.size();
    }

}
