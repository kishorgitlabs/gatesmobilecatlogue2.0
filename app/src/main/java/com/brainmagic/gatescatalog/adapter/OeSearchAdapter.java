package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brainmagic.gatescatalog.api.models.Sample;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class OeSearchAdapter extends BaseAdapter {

    private List<Sample> sampleList;
    private Context mContext;

    public OeSearchAdapter(Context mContext, List<Sample> sampleList) {
        this.sampleList = sampleList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return sampleList.size();
    }

    @Override
    public Object getItem(int position) {
        return sampleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.oe_search_view_layout, parent, false);

        TextView oem = convertView.findViewById(R.id.oem);
        TextView model = convertView.findViewById(R.id.model);
        TextView productType = convertView.findViewById(R.id.product_type);
        TextView segment = convertView.findViewById(R.id.segment);
        TextView engineSpecification = convertView.findViewById(R.id.engine_specification);
        TextView partNo = convertView.findViewById(R.id.part_no);


        String Oem = sampleList.get(position).getOem();
        String Model = sampleList.get(position).getModel();
        String ProductType = sampleList.get(position).getProductType();
        String Segment = sampleList.get(position).getSegment();
        String EngineSpecification = sampleList.get(position).getEngineSpecification();
        String PartNo = sampleList.get(position).getPartNo();

        oem.setText(Oem);
        model.setText(Model);
        productType.setText(ProductType);
        segment.setText(Segment);
        engineSpecification.setText(EngineSpecification);
        partNo.setText(PartNo);

        return convertView;
    }
}
