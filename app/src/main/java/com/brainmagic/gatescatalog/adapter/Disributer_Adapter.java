package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.distributors.DistributorListResult;

import java.util.List;


/**
 * Created by BRAINMAGIC on 19-08-2016.
 */
public class Disributer_Adapter extends ArrayAdapter {


    Context context;
    List<DistributorListResult> distributorListResults;


    // AddressResultReceiver mResultReceiver;
//    int fetchType = Constants.USE_ADDRESS_LOCATION;
    String lat,lon;


    public Disributer_Adapter(Context context, List<DistributorListResult> distributorListResults) {
        super(context, R.layout.disributer_adapter, distributorListResults);

        //mResultReceiver = new AddressResultReceiver(null);

        this.context = context;
        this.distributorListResults = distributorListResults;


    }

    @Override
    public int getCount() {
        return distributorListResults.size();
    }

    public void setDistributorListResults(List<DistributorListResult> distributorListResults) {
        this.distributorListResults = distributorListResults;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Dis_OrderHolder holder;

        convertView = null;

        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.disributer_adapter, parent, false);

            holder = new Dis_OrderHolder();
            TextView dis_country = (TextView) convertView.findViewById(R.id.dis_country);
            TextView dis_state = (TextView) convertView.findViewById(R.id.dis_state);
            TextView dis_city = (TextView) convertView.findViewById(R.id.dis_city);
            TextView dis_name = (TextView) convertView.findViewById(R.id.dis_name);
            TextView dis_con_person = (TextView) convertView.findViewById(R.id.dis_con_person);
            TextView dis_adress = (TextView) convertView.findViewById(R.id.dis_adress);
            TextView dis_pincode = (TextView) convertView.findViewById(R.id.dis_pincode);
            TextView dis_email = (TextView) convertView.findViewById(R.id.dis_email);
            TextView dis_llno = (TextView) convertView.findViewById(R.id.dis_llno);
            TextView dis_mobno = (TextView) convertView.findViewById(R.id.dis_mobno);
            ImageView dis_map = (ImageView) convertView.findViewById(R.id.dis_map);
            //TextView Adress = (TextView) convertView.findViewById(R.id.Adress);
            //  TextView  adapter_oepart_no = (TextView) convertView.findViewById(R.id.adapter_oe_part_no);
            //  TextView  adapter_mrp = (TextView) convertView.findViewById(R.id.adapter_mrp);
            //  ImageView adapter_image = (ImageView) convertView.findViewById(R.id.adapter_image);
            //  ImageView adapter_more_info = (ImageView) convertView.findViewById(R.id.adapter_more_info);
            //  zoom = (ImageView) ((Activity) context).findViewById(R.id.zoomimage);

            try {

                dis_country.setText(distributorListResults.get(position).getCountry()==null?"NA":distributorListResults.get(position).getCountry());
                dis_state.setText(distributorListResults.get(position).getState()==null?"NA":distributorListResults.get(position).getState());
                dis_city.setText(distributorListResults.get(position).getCity()==null?"NA":distributorListResults.get(position).getCity());
                dis_name.setText(distributorListResults.get(position).getName()==null?"NA":distributorListResults.get(position).getName());
                dis_con_person.setText(distributorListResults.get(position).getContactperson()==null?"NA":distributorListResults.get(position).getContactperson());
                dis_adress.setText(distributorListResults.get(position).getAddress()==null?"NA":distributorListResults.get(position).getAddress());
                dis_pincode.setText(String.valueOf(distributorListResults.get(position).getPincode()==null?"NA":distributorListResults.get(position).getPincode()));
                dis_email.setText(distributorListResults.get(position).getEmail()==null?"NA":distributorListResults.get(position).getEmail().equals("NULL")?"NA":distributorListResults.get(position).getEmail());
                dis_llno.setText(distributorListResults.get(position).getLandline()==null?"NA":distributorListResults.get(position).getLandline());
                dis_mobno.setText(String.valueOf(distributorListResults.get(position).getContact()==null?"NA":distributorListResults.get(position).getContact()));

            }catch (Exception e)
            {
                e.printStackTrace();
            }
            //     adapter_mrp.setText(mrp.get(position));
            // adapter_oe_part_no.setText(oepart);
            //   adapter_image.setImageBitmap(imagelist.get(position));


//            dis_llno.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent l = new Intent(Intent.ACTION_DIAL);
//                    l.setData(Uri.parse("tel:" + landline.get(position)));
//                    context.startActivity(l);
//                }
//            });
//
//
//            dis_mobno.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent l = new Intent(Intent.ACTION_DIAL);
//                    l.setData(Uri.parse("tel:" + mobno.get(position)));
//                    context.startActivity(l);
//                }
//            });
//
//            dis_email.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(!TextUtils.isEmpty(email.get(position))) {
//                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
//                                "mailto", email.get(position), null));
//                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
//                        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
//                        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
//                    }
//                }
//            });


            dis_map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    getLocationFromAddress(distributorListResults.get(position).getAddress());
//                    Intent a=new Intent();
//                    a.putExtra("LAT",lat);
//                    a.putExtra("LONG",lon);
//                    a.putExtra("ADRESS",adress.get(position).toString()+ "\n" +city_list.get(position).toString()+ "\n" + state_list.get(position).toString()+ "\n" +country_list.get(position).toString());
//                    context.startActivity(a);

                    Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + distributorListResults.get(position).getAddress()+","+distributorListResults.get(position).getCity()+","+ distributorListResults.get(position).getState()+","+distributorListResults.get(position).getCountry()));
                    context.startActivity(searchAddress);
                }
            });


            convertView.setTag(holder);
        } else {
            holder = (Dis_OrderHolder) convertView.getTag();
        }


        return convertView;
    }


    public void getLocationFromAddress(String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;


        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address == null) {
                return;
            }
            Address location = address.get(0);
            lat = Double.toString(location.getLatitude());
            lon = Double.toString(location.getLongitude());

            Log.v("lat",lat+"lon"+lon);

        }catch (Exception ex){

        }
    }
}


class Dis_OrderHolder {
    public TextView sno;
    public TextView orderno;
    public TextView datetxt;
    public TextView delarname;


}


