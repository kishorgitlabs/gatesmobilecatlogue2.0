package com.brainmagic.gatescatalog.adapter.threeepandablelist;

import android.content.Context;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.brainmagic.gatescatalog.alertbox.Alertbox;

@SuppressWarnings("unchecked")
public class CustomExpListView extends ExpandableListView {


    public CustomExpListView(Context context) {
        super(context);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(1200, MeasureSpec.AT_MOST);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(20000, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "expandable list view: "+e.toString(), Toast.LENGTH_SHORT).show();

        }
    }
}