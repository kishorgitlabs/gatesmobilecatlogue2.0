package com.brainmagic.gatescatalog.adapter;

public class Data{
	private int trainingattendance;
	private int flangeinstall;
	private String city;
	private int weeklyquiz;
	private int carpasting;
	private String mobile;
	private int joiningfacebook;
	private String name;
	private int trainingassesment;
	private int welcomebonus;
	private int monthlyperformer;
	private String state;
	private int totpoints;

	public void setTrainingattendance(int trainingattendance){
		this.trainingattendance = trainingattendance;
	}

	public int getTrainingattendance(){
		return trainingattendance;
	}

	public void setFlangeinstall(int flangeinstall){
		this.flangeinstall = flangeinstall;
	}

	public int getFlangeinstall(){
		return flangeinstall;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setWeeklyquiz(int weeklyquiz){
		this.weeklyquiz = weeklyquiz;
	}

	public int getWeeklyquiz(){
		return weeklyquiz;
	}

	public void setCarpasting(int carpasting){
		this.carpasting = carpasting;
	}

	public int getCarpasting(){
		return carpasting;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setJoiningfacebook(int joiningfacebook){
		this.joiningfacebook = joiningfacebook;
	}

	public int getJoiningfacebook(){
		return joiningfacebook;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTrainingassesment(int trainingassesment){
		this.trainingassesment = trainingassesment;
	}

	public int getTrainingassesment(){
		return trainingassesment;
	}

	public void setWelcomebonus(int welcomebonus){
		this.welcomebonus = welcomebonus;
	}

	public int getWelcomebonus(){
		return welcomebonus;
	}

	public void setMonthlyperformer(int monthlyperformer){
		this.monthlyperformer = monthlyperformer;
	}

	public int getMonthlyperformer(){
		return monthlyperformer;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setTotpoints(int totpoints){
		this.totpoints = totpoints;
	}

	public int getTotpoints(){
		return totpoints;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"trainingattendance = '" + trainingattendance + '\'' + 
			",flangeinstall = '" + flangeinstall + '\'' + 
			",city = '" + city + '\'' + 
			",weeklyquiz = '" + weeklyquiz + '\'' + 
			",carpasting = '" + carpasting + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",joiningfacebook = '" + joiningfacebook + '\'' + 
			",name = '" + name + '\'' + 
			",trainingassesment = '" + trainingassesment + '\'' + 
			",welcomebonus = '" + welcomebonus + '\'' + 
			",monthlyperformer = '" + monthlyperformer + '\'' + 
			",state = '" + state + '\'' + 
			",totpoints = '" + totpoints + '\'' + 
			"}";
		}
}
