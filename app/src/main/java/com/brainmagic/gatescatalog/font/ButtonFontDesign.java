
package com.brainmagic.gatescatalog.font;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import androidx.annotation.RequiresApi;

public class ButtonFontDesign extends Button {

    private Context context;

    public ButtonFontDesign(Context context) {
        super(context);
        isInEditMode();
    }

    public ButtonFontDesign(Context context, AttributeSet attrs) {
        super(context, attrs);
        isInEditMode();
    }

    public ButtonFontDesign(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        isInEditMode();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Typeface setTypeface() {
        Typeface normalTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Geogtq-Md.otf");
        return normalTypeface;

    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        if (!isInEditMode()) {
            Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Geogtq-Md.otf");
            Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Geogtq-Sb.otf");
            if (style == 1) {
                super.setTypeface(boldTypeface);
            } else {
                super.setTypeface(normalTypeface);
            }
        }
    }
}