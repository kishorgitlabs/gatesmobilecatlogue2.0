package com.brainmagic.gatescatalog.services;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.activities.HomeScreenActivity;
import com.brainmagic.gatescatalog.activities.RegisterActivity;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.background.Backgroundpojo;
import com.brainmagic.gatescatalog.api.models.logdetail.LogDetailModel;
import com.brainmagic.gatescatalog.api.models.sendlog.SendLog;
import com.brainmagic.gatescatalog.api.models.webotpverify.WebOTPVerify;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackgroundService extends IntentService {
    public BackgroundService() {
        super("");
    }

    public BackgroundService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        try {
            Network_Connection_Activity connection = new Network_Connection_Activity(getApplicationContext());
            if (connection.CheckInternet()) {

                String segment = intent.getStringExtra("segmentLog");
                String name = intent.getStringExtra("nameLog");
                String vehicleName = intent.getStringExtra("vehicleNameLog");
                String partNo = intent.getStringExtra("partNoLog");
                String mobileNo = intent.getStringExtra("mobileNoLog");
                String userType = intent.getStringExtra("userTypeLog");
                String state = intent.getStringExtra("stateLog");
                String city = intent.getStringExtra("cityLog");
                String zipCode = intent.getStringExtra("zipCodeLog");
                String location = intent.getStringExtra("locationLog");
                String deviceType = intent.getStringExtra("deviceTypeLog");



                try {
                    APIService service = RetrofitClient.getApiService();

                    Call<Backgroundpojo> call = service.sendLog(segment,name,vehicleName,partNo,mobileNo,userType,state,city,zipCode,location,deviceType);
                    call.enqueue(new Callback<Backgroundpojo>() {
                        @Override
                        public void onResponse(Call<Backgroundpojo> call, Response<Backgroundpojo> response) {
                            try {
                                if (response.body().getResult().equals("Success")) {

                                    stopSelf();
                                    Toast.makeText(getApplicationContext(), "SUCESS", Toast.LENGTH_SHORT).show();
                                }
                                else if (response.body().getResult().equals("NotVerified")) {
                                    stopSelf();
                                    Toast.makeText(getApplicationContext(),"Failure",Toast.LENGTH_LONG).show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<Backgroundpojo> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                APIService service = RetrofitClient.getApiQualityService();
//
//                Call<SendLog> call = service.sendLog(segment, name, vehicleName, partNo, mobileNo, userType,
//                        state, city, zipCode, location, deviceType);
//
//                call.enqueue(new Callback<SendLog>() {
//                    @Override
//                    public void onResponse(Call<SendLog> call, Response<SendLog> response) {
//                        if (response.isSuccessful())
//                            stopSelf();
//                    }
//
//                    @Override
//                    public void onFailure(Call<SendLog> call, Throwable t) {
//                        stopSelf();
//                    }
//                });



            }
        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
        }

        return START_REDELIVER_INTENT;
    }
}
